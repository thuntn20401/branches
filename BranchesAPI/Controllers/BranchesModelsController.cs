﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BranchesAPI.Models;

namespace BranchesAPI.Controllers
{
    public class BranchesModelsController : Controller
    {
        private readonly BranchesContext _context;

        public BranchesModelsController(BranchesContext context)
        {
            _context = context;
        }

        // GET: BranchesModels
        public async Task<IActionResult> Index()
        {
              return _context.Branches != null ? 
                          View(await _context.Branches.ToListAsync()) :
                          Problem("Entity set 'BranchesContext.Branches'  is null.");
        }

        // GET: BranchesModels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Branches == null)
            {
                return NotFound();
            }

            var branchesModel = await _context.Branches
                .FirstOrDefaultAsync(m => m.BranchId == id);
            if (branchesModel == null)
            {
                return NotFound();
            }

            return View(branchesModel);
        }

        // GET: BranchesModels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: BranchesModels/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public async Task<IActionResult> Create([Bind("BranchId,Name,Address,City,State,ZipCode")] BranchesModel branchesModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(branchesModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(branchesModel);
        }

        // GET: BranchesModels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Branches == null)
            {
                return NotFound();
            }

            var branchesModel = await _context.Branches.FindAsync(id);
            if (branchesModel == null)
            {
                return NotFound();
            }
            return View(branchesModel);
        }

        // POST: BranchesModels/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BranchId,Name,Address,City,State,ZipCode")] BranchesModel branchesModel)
        {
            if (id != branchesModel.BranchId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(branchesModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BranchesModelExists(branchesModel.BranchId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(branchesModel);
        }

        // GET: BranchesModels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Branches == null)
            {
                return NotFound();
            }

            var branchesModel = await _context.Branches
                .FirstOrDefaultAsync(m => m.BranchId == id);
            if (branchesModel == null)
            {
                return NotFound();
            }

            return View(branchesModel);
        }

        // POST: BranchesModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Branches == null)
            {
                return Problem("Entity set 'BranchesContext.Branches'  is null.");
            }
            var branchesModel = await _context.Branches.FindAsync(id);
            if (branchesModel != null)
            {
                _context.Branches.Remove(branchesModel);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BranchesModelExists(int id)
        {
          return (_context.Branches?.Any(e => e.BranchId == id)).GetValueOrDefault();
        }
    }
}
