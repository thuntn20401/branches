﻿using Microsoft.EntityFrameworkCore;
namespace BranchesAPI.Models
{
    public class BranchesContext: DbContext
    {
        public BranchesContext(DbContextOptions<BranchesContext> options) : base(options)
        {
        }

        public DbSet<BranchesModel> Branches { get; set; } = null;
    }
}
