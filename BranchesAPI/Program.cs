using Microsoft.EntityFrameworkCore;
using BranchesAPI.Models;
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDbContext<BranchesContext>(opt => opt.UseInMemoryDatabase("Branches"));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapGet("/BranchesAPI", () =>
{
    var branches = Enumerable.Range(1, 10).Select(index =>
    new BranchesModel
    {
        BranchId = index,
        Name = $"{index}"
    }
    ).ToArray();
    return branches;
}).WithName("GetBraches");


app.Run();
